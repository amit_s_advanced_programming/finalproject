#include "Game.h"



Game::Game(const std::vector<User*>& players, int questionsNo, DataBase& db)
	: _players(players), _isGameEnded(false), _questions_no(questionsNo)
{
	this->_db = new DataBase(db);

	if (!this->insertGameToDB())
		throw std::string("Can not open DB.");

	this->initQuestionsFromDB();
	for (auto player : this->_players)
	{
		player->setGame(this);
		this->_results[player->getUsername()] = 0;
	}
}

Game::~Game()
{
	for (auto q : _questions)
		delete q;

	// TODO : delete players?????
}

void Game::sendFirstQuestion()
{
	this->sendQuestionToAllUsers();
}
	
void Game::handleFinishGame()
{
	this->_db->updateGameStatus(this->getID());
	this->sendMessageToAllUsers(this->buildFinishGameMessage());

	for (auto player : this->_players)
		player->setGame(nullptr);
}

bool Game::handleNextTurn()
{
	if (this->_players.empty())
	{
		this->handleFinishGame();
		return false;
	}

	if (this->_currentTurnAnswers + 1 == this->_players.size())
		return false;

	if (_currQuestioIndex == this->_questions.size() - 1)
	{
		this->handleFinishGame();
		return true;
	}

	this->_currQuestioIndex++;
	this->sendQuestionToAllUsers();

	return true;
}

bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{
	Question* currQuestion = this->_questions[this->_currQuestioIndex];
	bool isCorrectAnswer = answerNo - 1 == currQuestion->getCorrectAnswerIndex();
	this->_currentTurnAnswers++;

	if (isCorrectAnswer)
		_results[user->getUsername()]++;

	std::string userAnswer = (answerNo == 5) ? "" : currQuestion->getAnswers()[answerNo - 1];

	this->_db->addAnswerToPlayer
	(this->getID(), user->getUsername(), currQuestion->getId(), userAnswer, isCorrectAnswer, time);


	// Send 120
	user->send(Helper::buildMessage(MT_SERVER_USER_ANSER_FEEDBACK, isCorrectAnswer ? 1 : 0));

	this->handleNextTurn();
	return true;
}

bool Game::leaveGame(User * user)
{
	if (std::find(this->_players.begin(), this->_players.end(), user) != this->_players.end())
	{
		user->leaveGame();
		user->setGame(nullptr);
	}

	return true;
}

int Game::getID()
{
	return this->_id;
}

bool Game::insertGameToDB()
{
	int id = _db->insertNewGame();
	this->_id = id;

	return id != -1;
}

void Game::initQuestionsFromDB()
{
	this->_questions = this->_db->initQuestions(this->_questions_no);
}

void Game::sendQuestionToAllUsers()
{
	this->_currentTurnAnswers = 0;
	this->sendMessageToAllUsers(this->buildQuestsionMessage());
}

void Game::sendMessageToAllUsers(std::string msg)
{
	for (auto player : this->_players)
	{
		try
		{
			player->send(msg);
		}
		catch (...) {}
	}
}

std::string Game::buildQuestsionMessage()
{
	std::string* answers = nullptr;
	std::stringstream ss;
	ss << MT_SERVER_START_GAME;

	auto q = this->_questions[this->_currQuestioIndex];
	ss << Helper::getPaddedNumber((int)q->getQuestion().size(), 3);
	ss << q->getQuestion();
	answers = q->getAnswers();

	for (int i = 0; i < ANSWERS_NO; i++)
	{
		ss << Helper::getPaddedNumber((int)answers[i].size(), 3);
		ss << answers[i];
	}

	std::cout << ss.str() << std::endl;

	return ss.str();
}

std::string Game::buildFinishGameMessage() {
	std::stringstream ss;
	ss << MT_SERVER_FINISH_GAME;
	ss << this->_results.size();

	for (auto const &result : this->_results) {
		std::string name = std::string(result.first.c_str());
		int nameSize = name.size();
		int score = result.second;

		ss << Helper::getPaddedNumber(nameSize, 2) <<
			name <<
			Helper::getPaddedNumber(score, 2);
	}

	return ss.str();
}
