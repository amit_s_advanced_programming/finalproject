#include "RecievedMessage.h"


RecievedMessage::RecievedMessage(SOCKET sock, int msgCode)
	: _sock(sock), _messageCode(msgCode)
{}

RecievedMessage::RecievedMessage(SOCKET sock, int msgCode, std::vector<std::string> values)
	: _sock(sock), _messageCode(msgCode), _values(values)
{}

RecievedMessage::~RecievedMessage()
{}

SOCKET RecievedMessage::getSocket()
{
	return this->_sock;
}

User * RecievedMessage::getUser()
{
	return this->_user;
}

void RecievedMessage::setUser(User * user)
{
	this->_user = user;
}

int RecievedMessage::getMessageCode()
{
	return this->_messageCode;
}

std::vector<std::string>& RecievedMessage::getValues()
{
	return this->_values;
}


