#pragma once
#pragma comment(lib,"ws2_32")

#include "WinSock2.h"
#include <vector>
#include <map>
#include <mutex>
#include <queue>
#include <iostream>

#include "User.h"
#include "Room.h"
#include "RecievedMessage.h"
#include "DataBase.h"
#include "Helper.h"
#include "Protocol.h"
#include "Validator.h"

class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();

	void server();

private:
	SOCKET _sock;
	std::map<SOCKET, User*> _connectedUsers;
	DataBase _db;
	std::map<int, Room*> _roomsList;
	std::mutex _mtxRecieveMessage;
	std::queue<RecievedMessage*> _queRcvMessages;
	std::condition_variable _msgQueueCondition;

	static int _roomIdSequence;

	static const unsigned short PORT = 8826;
	static const unsigned int IFACE = 0;

	void accept();
	void bindAndListen();
	void clientHandler(SOCKET sock);
	void safeDeleteUser(RecievedMessage* msg);

	// User
	User* handleSignin(RecievedMessage* msg);
	bool handleSignup(RecievedMessage* msg);
	void handleSignout(RecievedMessage* msg);

	// Game
	void handleLeaveGame(RecievedMessage* msg);
	bool handleStartGame(RecievedMessage* msg);
	void handlePlayerAnswer(RecievedMessage* msg);

	// Rooms
	bool handleCreateRoom(RecievedMessage* msg);
	bool handleCloseRoom(RecievedMessage* msg);
	bool handleJoinRoom(RecievedMessage* msg);
	std::string getRoomsList();
	bool handleLeaveRoom(RecievedMessage* msg);
	void handleGetUserInRoom(RecievedMessage* msg);
	void handleGetRooms(RecievedMessage* msg);

	// Score
	void handleGetBestScores(RecievedMessage* msg);
	void handleGetPersonalStatus(RecievedMessage* msg);

	void handleRecievedMessages();
	void addRecievedMessage(RecievedMessage* msg);
	RecievedMessage* buildRecievedMessage(SOCKET sock, int);

	User* getUserByName(std::string username);
	User* getUserBySocket(SOCKET sock);
	Room* getRoomById(int id);
};
