#pragma once

#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <exception>
#include <algorithm>

#include "User.h"
#include "Helper.h"
#include "Question.h"
#include "DataBase.h"

class User;

class Game
{
public:
	Game(const std::vector<User*>& players, int questionsNo, DataBase& db);
	~Game();

	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int answerNo, int time);
	bool leaveGame(User* users);
	int getID();


private:
	std::vector<Question*> _questions;
	std::vector<User*> _players;
	int _questions_no;
	int _currQuestioIndex;
	DataBase* _db;
	std::map<std::string, int> _results;
	int _currentTurnAnswers;
	bool _isGameEnded;
	int _id;

	bool insertGameToDB();
	void initQuestionsFromDB();
	void sendQuestionToAllUsers();
	void sendMessageToAllUsers(std::string msg);
	std::string buildQuestsionMessage();
	std::string buildFinishGameMessage();

};

