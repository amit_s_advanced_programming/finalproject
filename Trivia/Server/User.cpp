#include "User.h"



User::User(std::string username, SOCKET sock)
	: _username(username), _sock(sock),
	_currGame(nullptr), _currRoom(nullptr)
{
}

User::~User()
{
}

void User::send(std::string msg)
{
	// TODO: make sure you don't miss something.
	Helper::sendData(this->getSocket(), msg);
	TRACE("Message sent to user: %s, msg: %s", this->getUsername().c_str(), msg.c_str());
}

std::string User::getUsername()
{
	return std::string(this->_username);
}

SOCKET User::getSocket()
{
	return this->_sock;
}

Room * User::getRoom()
{
	return this->_currRoom;
}

Game * User::getGame()
{
	return this->_currGame;
}

void User::setGame(Game * game)
{
	this->_currGame = game;
	this->_currRoom = nullptr;
}

void User::clearRoom()
{
	this->_currGame = nullptr;
}

bool User::createRoom(int roomId, std::string roomName, int maxUsers, int questionsNo, int questionTime)
{
	if (this->_currRoom)
	{
		// Fail msg
		this->send(Helper::buildMessage(MT_SERVER_ROOM_CREATE, 1));
		return false;
	}

	Room* newRoom = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime);
	this->send("1140");

	this->joinRoom(newRoom);

	return true;
}

bool User::joinRoom(Room * room)
{
	if (this->_currRoom)
		return false;

	if (room->joinRoom(this))
	{
		this->_currRoom = room;
		return true;
	}

	return false;
}

void User::leaveRoom()
{
	this->getRoom()->leaveRoom(this);
	this->_currRoom = nullptr;
	this->send(std::to_string(MT_SERVER_LEAVE_ROOM_SUCC));
}

int User::closeRoom()
{
	int r = this->_currRoom->closeRoom(this);
	if (r != -1)
		this->_currRoom = nullptr;

	return r;
}

bool User::leaveGame()
{
	this->setGame(nullptr);
	return false;
}
