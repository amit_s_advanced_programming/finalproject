#pragma once
#pragma comment(lib,"ws2_32")

#include "WinSock2.h"
#include <string>

#include "Helper.h"
#include "Room.h"
#include "Game.h"

class Game;
class User
{
public:
	User(std::string username, SOCKET sock);
	~User();

	void send(std::string msg);
	
	std::string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	
	void setGame(Game* game);
	void clearRoom();
	bool createRoom(int roomId, std::string roomName, int maxUsers, int questionsNo, int questionTime);
	bool joinRoom(Room* room);
	void leaveRoom();
	int  closeRoom();
	bool leaveGame();

private:
	std::string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;
};

