#include "Room.h"
#include "User.h" // Solve ciruclar include

Room::Room(int id, User * admin, std::string name, int maxUsers, int questionsNo, int questionTime)
	: _id(id), _admin(admin), _name(name), _maxUsers(maxUsers), _questionsNo(questionsNo),
	_questionTime(questionTime)
{
}

Room::~Room()
{
}

bool Room::joinRoom(User * user)
{
	if (_maxUsers == this->_users.size())
	{
		user->send(Helper::buildMessage(MT_SERVER_ROOM_JOIN, 1));
		return false;
	}

	this->_users.push_back(user);
	TRACE("Added user: %s to room: %s (%d)", user->getUsername().c_str(), this->getName().c_str(), this->getId());

	std::stringstream ss;
	ss << MT_SERVER_ROOM_JOIN <<
		"0" <<
		Helper::getPaddedNumber(this->_questionsNo, 2) <<
		Helper::getPaddedNumber(this->_questionTime, 2);

	user->send(ss.str());
	this->sendMessage(this->getUsersListMessage());

	return true;
}

void Room::leaveRoom(User * user)
{
	this->_users.erase(std::find(this->_users.begin(), this->_users.end(), user));
	this->sendMessage(this->getUsersListMessage());
}

int Room::closeRoom(User * user)
{
	if (user != this->_admin)
		return -1;

	this->sendMessage(std::to_string(MT_SERVER_CLOSE_ROOM));

	for (auto _user : this->_users)
		if (_user != user)
			_user->clearRoom();

	return this->getId();
}

std::vector<User*> Room::getUsers()
{
	return this->_users;
}

std::string Room::getUsersListMessage()
{
	std::stringstream ss;
	ss << MT_SERVER_ROOM_USERS_LIST <<
		this->_users.size();

	for (auto user : this->_users)
		ss << Helper::getPaddedNumber((int) user->getUsername().size(), 2) <<
			user->getUsername();
	 

	return ss.str();
}

int Room::getQuestionsNo()
{
	return this->_questionsNo;
}

int Room::getId()
{
	return this->_id;
}

int Room::getQuestionTime()
{
	return this->_questionTime;
}

std::string Room::getName()
{
	return this->_name;
}

void Room::sendMessage(std::string msg)
{
	this->sendMessage(nullptr, msg);
}

void Room::sendMessage(User* excludeUser, std::string msg)
{
	for (auto user : _users)
		if (user != excludeUser)
			user->send(msg);
}
