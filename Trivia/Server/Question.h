#pragma once

#include <cstdlib> 
#include <string>
#include <ctime> 

#define ANSWERS_NO (4)

class Question
{
public:
	Question(int id, std::string question, std::string correctAnswer, std::string answer2, std::string answer3, std::string answer4);
	~Question();

	std::string getQuestion();
	std::string* getAnswers();
	int getCorrectAnswerIndex();
	int getId();

private:
	std::string _question;
	std::string _answers[ANSWERS_NO];
	int _correctAnswerIndex;
	int _id;
};

