#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "TriviaServer.h"
#include <iostream>
#include "Validator.h"

int main()
{
	try
	{
		WSAInitializer wsa_init;
		TriviaServer md_server;
		md_server.server();
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was thrown in function: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Unknown exception in main !" << std::endl;
	}

	return (0);
}