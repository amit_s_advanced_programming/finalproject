#include "TriviaServer.h"

int TriviaServer::_roomIdSequence = 0;

TriviaServer::TriviaServer()
{
	this->_sock = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->_sock == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}


TriviaServer::~TriviaServer()
{
	TRACE(__FUNCTION__ " closing accepting socket");
	// why is this try necessarily ?
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(this->_sock);
	}
	catch (...) {}
}

void TriviaServer::server()
{
	bindAndListen();

	// create new thread for handling message
	std::thread tr(&TriviaServer::handleRecievedMessages, this);
	tr.detach();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		TRACE("accepting a client...");
		this->accept();
	}
}

void TriviaServer::accept()
{
	SOCKET client_socket = WIN32::accept(this->_sock, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	TRACE("Client accepted !");
	// create new thread for client	and detach from it
	std::thread tr(&TriviaServer::clientHandler, this, client_socket);
	tr.detach();
}

void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;


	// again stepping out to the global namespace
	if (::bind(this->_sock, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	if (::listen(this->_sock, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");

	TRACE("Binded and listening\n");
}

void TriviaServer::clientHandler(SOCKET sock)
{
	RecievedMessage* currMsg = nullptr;

	try
	{
		// Get the first message code
		int msgCode = Helper::getMessageTypeCode(sock);
		while (msgCode != MT_CLIENT_EXIT && msgCode != 0)
		{
			currMsg = buildRecievedMessage(sock, msgCode);
			addRecievedMessage(currMsg);

			msgCode = Helper::getMessageTypeCode(sock);
		}

		currMsg = buildRecievedMessage(sock, MT_CLIENT_EXIT);
		addRecievedMessage(currMsg);
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was catch in function clientHandler. socket=" << sock << ", what=" << e.what() << std::endl;
		currMsg = buildRecievedMessage(sock, MT_CLIENT_EXIT);
		addRecievedMessage(currMsg);
	}

	closesocket(sock);
}

void TriviaServer::safeDeleteUser(RecievedMessage * msg)
{
	try {
		this->handleSignout(msg);
	}
	catch (...) {}
	
}

User * TriviaServer::handleSignin(RecievedMessage * msg)
{
	std::string username = msg->getValues()[0],
		password = msg->getValues()[1];
	int serverMsgCode = MT_SERVER_SIGN_IN_SUCC;
	User* user = nullptr;

	if (!_db.isUserAndPassMatch(username, password))
		serverMsgCode = MT_SERVER_SIGN_IN_WRONG_DETAIL;
	else if (this->getUserByName(username))
		serverMsgCode = MT_SERVER_SIGN_IN_ALREADY_CONECTED;
	else
	{
		auto user = new User(username, msg->getSocket());
		_connectedUsers[msg->getSocket()] = user;
		TRACE("Adding new user to connected users list: socket = %lld, username = %s", user->getSocket(), user->getUsername().c_str());
	}

	// Notify the user
	TRACE("Log in message sent to: %lld, msg code: %d", msg->getSocket(), serverMsgCode);
	Helper::sendData(msg->getSocket(), std::to_string(serverMsgCode));
	return user;
}

bool TriviaServer::handleSignup(RecievedMessage * msg)
{
	std::string username = msg->getValues()[0],
		password = msg->getValues()[1], email = msg->getValues()[2];
	int serverMsgCode = MT_SERVER_SIGN_UP_SUCC;

	// Check whether the password is valid
	if (!Validator::isPasswordValid(password))
		serverMsgCode = MT_SERVER_SIGN_UP_PASS_ILLEGAL;

	// Check whether the username is valid
	else if (!Validator::isUsernameValid(username))
		serverMsgCode = MT_SERVER_SIGN_UP_USER_ILLEGAL;

	// Check whether the username already exists
	else if (this->_db.isUserExists(username))
		serverMsgCode = MT_SERVER_SIGN_UP_USER_EXISTS;

	// Add the user to the DataBase (Or to the map, for now).
	else
	{
		this->_db.addNewUser(username, password, email);
		TRACE("User signed up successfuly: username: %s, socket = %lld", username.c_str(), msg->getSocket());
	}

	// Notify the user
	Helper::sendData(msg->getSocket(), std::to_string(serverMsgCode));

	return (serverMsgCode == MT_SERVER_SIGN_UP_SUCC);
}

void TriviaServer::handleSignout(RecievedMessage * msg)
{
	User* user = msg->getUser();

	if (!user) return;
	this->handleCloseRoom(msg);
	this->handleLeaveRoom(msg);
	this->handleLeaveGame(msg);

	this->_connectedUsers.erase(this->_connectedUsers.find(msg->getSocket()));
}

bool TriviaServer::handleLeaveRoom(RecievedMessage * msg)
{
	if (!msg->getUser())
		return false;

	if (!msg->getUser()->getRoom())
		return false;

	msg->getUser()->leaveRoom();

	return true;
}

void TriviaServer::handleGetUserInRoom(RecievedMessage * msg)
{
	if (!msg->getUser()) return;

	int roomId = std::stoi(msg->getValues()[0]);
	Room* room = getRoomById(roomId);

	if (!room)
	{
		msg->getUser()->send(Helper::buildMessage(MT_SERVER_ROOM_USERS_LIST, 0));
		return;
	}

	msg->getUser()->send(room->getUsersListMessage());
}

void TriviaServer::handleRecievedMessages()
{
	int msgCode = 0;
	SOCKET client_socket = NULL;

	while (true)
	{
		try
		{
			std::unique_lock<std::mutex> lck(this->_mtxRecieveMessage);

			// Wait for clients to enter the queue.
			if (this->_queRcvMessages.empty())
				_msgQueueCondition.wait(lck);

			// in case the queue is empty.
			if (this->_queRcvMessages.empty())
				continue;

			RecievedMessage* currMessage = _queRcvMessages.front();
			_queRcvMessages.pop();
			lck.unlock();

			// Extract the data from the tuple.
			client_socket = currMessage->getSocket();
			msgCode = currMessage->getMessageCode();

			TRACE("--------------------------------------\n");
			TRACE(__FUNCTION__ ": msgCode = %d, client_socket: %lld", msgCode, client_socket);

			switch (msgCode)
			{
			case MT_CLIENT_SIGN_UP:
				handleSignup(currMessage);
				break;
			case MT_CLIENT_LOG_IN:
				handleSignin(currMessage);
				break;
			case MT_CLIENT_CREATE_ROOM:
				handleCreateRoom(currMessage);
				break;
			case MT_CLIENT_JOIN_ROOM:
				handleJoinRoom(currMessage);
				break;
			case MT_CLIENT_GET_ROOMS:
				handleGetRooms(currMessage);
				break;
			case MT_CLIENT_LEAVE_ROOM:
				handleLeaveRoom(currMessage);
				break;
			case MT_CLIENT_CLOSE_ROOM:
				handleCloseRoom(currMessage);
				break;
			case MT_CLIENT_USERS_IN_ROOM:
				handleGetUserInRoom(currMessage);
				break;
			case MT_CLIENT_START_GAME:
				handleStartGame(currMessage);
				break;
			case MT_CLIENT_USER_ANSWER:
				handlePlayerAnswer(currMessage);
				break;
			case MT_CLIENT_LEAVE_GAME:
				handleLeaveGame(currMessage);
				break;
			case MT_CLIENT_SIGN_OUT:
				safeDeleteUser(currMessage);
				break;
			case MT_CLIENT_BEST_SCORES:
				handleGetBestScores(currMessage);
				break;
			case MT_CLIENT_PERSONAL_STATUS:
				handleGetPersonalStatus(currMessage);
				break;
			}

			TRACE("--------------------------------------\n");
			delete currMessage;
		}
		catch (...)
		{
			TRACE("Unknown Error.\n");
		}
	}


}

void TriviaServer::addRecievedMessage(RecievedMessage * msg)
{
	std::unique_lock<std::mutex> lck(this->_mtxRecieveMessage);
	this->_queRcvMessages.push(msg);
	lck.unlock();
	_msgQueueCondition.notify_all();

}

RecievedMessage * TriviaServer::buildRecievedMessage(SOCKET sock, int msgCode)
{
	std::vector<std::string> values;

	switch (msgCode)
	{
	case MT_CLIENT_SIGN_UP:
		// Push the username, password and email.
		values.push_back(Helper::getStringPartFromSockeBySizeSize(sock, 2));
		values.push_back(Helper::getStringPartFromSockeBySizeSize(sock, 2));
		values.push_back(Helper::getStringPartFromSockeBySizeSize(sock, 2));
		break;

	case MT_CLIENT_LOG_IN:
		// Push the username and psw
		values.push_back(Helper::getStringPartFromSockeBySizeSize(sock, 2));
		values.push_back(Helper::getStringPartFromSockeBySizeSize(sock, 2));
		break;

	case MT_CLIENT_CREATE_ROOM:
		// Push the name, num of players, num of q and time for a.
		values.push_back(Helper::getStringPartFromSockeBySizeSize(sock, 2));
		values.push_back(Helper::getStringPartFromSocket(sock, 1));
		values.push_back(Helper::getStringPartFromSocket(sock, 2));
		values.push_back(Helper::getStringPartFromSocket(sock, 2));
		break;

	case MT_CLIENT_JOIN_ROOM:
		// Push the room id
		values.push_back(Helper::getStringPartFromSocket(sock, 4));
		break;

	case MT_CLIENT_USERS_IN_ROOM:
		// Push the room id
		values.push_back(Helper::getStringPartFromSocket(sock, 4));
		break;

	case MT_CLIENT_USER_ANSWER:
		// Push answerNo and time
		values.push_back(Helper::getStringPartFromSocket(sock, 1));
		values.push_back(Helper::getStringPartFromSocket(sock, 2));
		break;
	}

	auto rMsg = new RecievedMessage(sock, msgCode, values);
	if (this->_connectedUsers.find(sock) != _connectedUsers.end())
		rMsg->setUser(this->_connectedUsers[sock]);

	return rMsg;
}

User * TriviaServer::getUserByName(std::string username)
{
	for (std::pair<SOCKET, User*> user : _connectedUsers) {
		if (user.second->getUsername() == username)
			return user.second;
	}

	return nullptr;
}

Room * TriviaServer::getRoomById(int id)
{
	if (this->_roomsList.find(id) != this->_roomsList.end())
		return this->_roomsList[id];

	return nullptr;
}


void TriviaServer::handleLeaveGame(RecievedMessage * msg)
{
	if (!msg->getUser()) return;
	if (!msg->getUser()->getGame()) return;

	if (!msg->getUser()->getGame()->leaveGame(msg->getUser()))
		delete msg->getUser()->getGame();
}

bool TriviaServer::handleStartGame(RecievedMessage * msg)
{
	if (!msg->getUser())
		return false;

	Room* room = msg->getUser()->getRoom();
	if (!room)
		return false;

	try
	{
		Game* game = new Game(room->getUsers(), room->getQuestionsNo(), this->_db);

		// Remove the room from the rooms list
		for (auto it = this->_roomsList.begin(); it != this->_roomsList.end();)
			if (it->second == room)
			{
				this->_roomsList.erase(it);
				break;
			}
			else
				++it;

		game->sendFirstQuestion();
	}
	catch (std::string& e) {
		msg->getUser()->send(e);
	}

	return true;
}

void TriviaServer::handlePlayerAnswer(RecievedMessage * msg)
{
	if (!msg->getUser())
		return;

	Game* game = msg->getUser()->getGame();
	if (!game)
		return;

	if (!game->handleAnswerFromUser(msg->getUser(), std::stoi(msg->getValues()[0]), std::stoi(msg->getValues()[1])))
		delete game;
}

bool TriviaServer::handleCreateRoom(RecievedMessage * msg)
{
	if (msg->getUser() == nullptr) return false;

	std::string roomName = msg->getValues()[0];
	int maxUsers = std::stoi(msg->getValues()[1]),
		questionsNo = std::stoi(msg->getValues()[2]),
		questionTime = std::stoi(msg->getValues()[3]);

	if (msg->getUser()->createRoom(_roomIdSequence, roomName, maxUsers, questionsNo, questionTime))
	{
		this->_roomsList[this->_roomIdSequence] = msg->getUser()->getRoom();
		Room *room = this->_roomsList[this->_roomIdSequence];
		TRACE("Room was created: ID = %d, Name: %s, Admin: %s, Num of player: %zd, Num of questions: %d, Answer Time: %d\n",
			room->getId(), roomName.c_str(), msg->getUser()->getUsername().c_str(), room->getUsers().size(), room->getQuestionsNo(), room->getQuestionTime());
		this->_roomIdSequence++;
		return true;
	}

	return false;
}

bool TriviaServer::handleCloseRoom(RecievedMessage * msg)
{
	if (!msg->getUser())
		return false;

	if (!msg->getUser()->getRoom())
		return false;

	int r = msg->getUser()->closeRoom();

	if (r == -1)
		return false;

	delete this->_roomsList[r];
	this->_roomsList[r] = nullptr;

	return true;
}

bool TriviaServer::handleJoinRoom(RecievedMessage * msg)
{
	int id = std::stoi(msg->getValues()[0]);

	if (!msg->getUser())
	{
		TRACE("Can't find any related user to join to %d", id);
		return false;
	}

	Room* room = this->getRoomById(id);
	if (!room)
	{
		TRACE("The room %d is not exists", id);
		msg->getUser()->send(Helper::buildMessage(MT_SERVER_ROOM_JOIN, 2));
		return false;
	}

	return msg->getUser()->joinRoom(room);
}

std::string TriviaServer::getRoomsList() {
	std::stringstream ss;

	ss << MT_SERVER_GET_ROOMS;
	ss << Helper::getPaddedNumber((int)this->_roomsList.size(), 4);

	for (auto& kv : this->_roomsList) {
		ss << Helper::getPaddedNumber(kv.second->getId(), 4);
		ss << Helper::getPaddedNumber((int)kv.second->getName().size(), 2);
		ss << kv.second->getName();
	}

	return ss.str();
}

void TriviaServer::handleGetRooms(RecievedMessage * msg)
{
	if (!msg->getUser())
		TRACE("Can't find any related user client_socket: %lld\n", msg->getSocket());

	msg->getUser()->send(this->getRoomsList());
}

void TriviaServer::handleGetBestScores(RecievedMessage * msg)
{
	const auto user = msg->getUser();
	if (!user) return;

	const auto scores = this->_db.getBestScores();
	for (std::pair<SOCKET, User*> user : _connectedUsers)
		user.second->send(std::to_string(MT_SERVER_BEST_SCORES) + scores);
}

void TriviaServer::handleGetPersonalStatus(RecievedMessage * msg)
{
	auto user = msg->getUser();
	if (!user) return;

	std::string status = _db.getPersonalStatus(user->getUsername());
	user->send(std::to_string(MT_SERVER_PERSONAL_STATUS) + status);
}
