#include "Question.h"
#include <random>
static std::default_random_engine generator;
static 	std::uniform_int_distribution<int> distribution(0, ANSWERS_NO - 1);

Question::Question(int id, std::string question, std::string correctAnswer, std::string answer2, std::string answer3, std::string answer4)
	: _id(id), _question(question)
{
	// Assign it in a circular way
	int index = 1;
	int r = (distribution(generator));
	this->_correctAnswerIndex = r;

	this->_answers[(r + index++) % ANSWERS_NO] = answer2;
	this->_answers[(r + index++) % ANSWERS_NO] = answer3;
	this->_answers[(r + index++) % ANSWERS_NO] = answer4;
	this->_answers[r] = correctAnswer;
}

Question::~Question()
{
}

std::string Question::getQuestion()
{
	return this->_question;
}

std::string * Question::getAnswers()
{
	return this->_answers;
}

int Question::getCorrectAnswerIndex()
{
	return this->_correctAnswerIndex;
}

int Question::getId()
{
	return this->_id;
}
