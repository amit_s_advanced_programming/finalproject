#pragma once
#include <vector>
#include <string>
#include <sstream>

#include "Helper.h"
#include "Protocol.h"

class User; // Solve ciruclar include

class Room
{
public:
	Room(int id, User* _admin, std::string name, int maxUsers, int questionsNo, int questionTime);
	~Room();

	bool joinRoom(User* user);
	void leaveRoom(User* user);
	int closeRoom(User* user);

	std::vector<User*> getUsers();
	std::string getUsersListMessage();
	int getQuestionsNo();
	int getId();
	int getQuestionTime();
	std::string getName();

	std::string getUsersAsString(std::vector<User*>, User*);
	void sendMessage(std::string);
	void sendMessage(User*, std::string);

private:
	std::vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionsNo;
	std::string _name;
	int _id;
};
