#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <ctime>

#include "Question.h"
#include "sqlite3.h"
#include "Helper.h"

#define DB_PATH ("C:\\Users\\magshimim\\Desktop\\trivia.db")

class DataBase
{
public:
	DataBase();
	DataBase(DataBase& db);
	~DataBase();

	bool isUserExists(std::string username);
	bool addNewUser(std::string username, std::string psw, std::string email);
	bool isUserAndPassMatch(std::string username, std::string psw);
	std::vector<Question*> initQuestions(int);
	std::string getBestScores();
	std::string getPersonalStatus(std::string username);
	int insertNewGame();
	bool updateGameStatus(int gameId);
	bool addAnswerToPlayer(int gameId, std::string username, int questionId, std::string answer, bool isCorrect, int answerTime);

private:
	sqlite3 *_db;

	static std::vector<Question*> _questions;
	static std::string _tmpStr;

	std::string quotesql(const std::string& s);
	int countRows(std::string sql);
	bool executeSQL(std::string sql);
	static int callbackQuestions(void *, int argc, char **argv, char **azCol);

	
	static int callbackBestScores(void *, int argc, char **argv, char **azCol);
	static int callbackPersonalStatus(void *, int argc, char **argv, char **azCol);
};

