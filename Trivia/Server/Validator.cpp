#include "Validator.h"


bool Validator::isPasswordValid(std::string password)
{
	std::regex r(R"((?=\S+$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,})");
	return std::regex_match(password, r);
}

bool Validator::isUsernameValid(std::string username)
{
	std::regex r(R"(^[a-zA-Z][^\s]{1,})");
	return std::regex_match(username, r);
}
