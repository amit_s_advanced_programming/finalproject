#pragma once

#include <vector>
#include <string>
#include <WinSock2.h>

#include "User.h"

class RecievedMessage
{
public:
	RecievedMessage(SOCKET sock, int msgCode);
	RecievedMessage(SOCKET sock, int msgCode, std::vector<std::string> values);
	~RecievedMessage();

	SOCKET getSocket();
	User* getUser();
	void setUser(User* user);
	int getMessageCode();
	std::vector<std::string>& getValues();

private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	std::vector<std::string> _values;
};

