#include "DataBase.h"


std::vector<Question*> DataBase::_questions;
std::string DataBase::_tmpStr;

DataBase::DataBase()
{
	int rc = sqlite3_open(DB_PATH, &this->_db);
	if (rc)
		throw std::string("Can't open database: " + std::string(sqlite3_errmsg(this->_db)));

}

DataBase::DataBase(DataBase & db)
{
	this->_db = db._db;
}

DataBase::~DataBase()
{
	sqlite3_close(this->_db);
}

bool DataBase::isUserExists(std::string username)
{
	return this->countRows("SELECT * FROM t_users WHERE username = " + quotesql(username) + ";") > 0;
}

bool DataBase::addNewUser(std::string username, std::string psw, std::string email)
{
	const std::string sql = "INSERT INTO t_users (username, password, email) VALUES (" +
		quotesql(username) + "," +
		quotesql(psw) + "," +
		quotesql(email) + ");";

	return this->executeSQL(sql);
}

bool DataBase::isUserAndPassMatch(std::string username, std::string psw)
{
	const std::string sql = "SELECT * FROM t_users WHERE username=" +
		quotesql(username) +
		" AND password=" +
		quotesql(psw) +
		";";

	return this->countRows(sql) > 0;
}

std::vector<Question*> DataBase::initQuestions(int qNo)
{
	const std::string sql = "SELECT * FROM t_questions ORDER BY RANDOM() LIMIT " + std::to_string(qNo);
	sqlite3_exec(this->_db, sql.c_str(), callbackQuestions, 0, nullptr);

	return _questions;
}

std::string DataBase::getBestScores()
{
	_tmpStr = "";
	sqlite3_exec(this->_db, "SELECT COUNT(is_correct), username FROM t_players_answers WHERE is_correct=1 GROUP BY username  ORDER BY COUNT(is_correct) DESC LIMIT 3;", callbackBestScores, 0, nullptr);
	return this->_tmpStr;
}

std::string DataBase::getPersonalStatus(std::string username)
{
	_tmpStr = "";
	std::stringstream ss;
	ss << "SELECT AVG(avg_time), SUM(num_of_correct_answers), SUM(num_of_wrong_answers), COUNT(*) FROM (SELECT COUNT(is_correct) as num_of_questions , SUM(is_correct) as num_of_correct_answers, AVG(answer_time) as avg_time, (COUNT(is_correct) - SUM(is_correct)) as num_of_wrong_answers FROM t_players_answers WHERE username='" <<
		username << "' GROUP BY game_id);";
	sqlite3_exec(this->_db, ss.str().c_str(),
		callbackPersonalStatus, 0, nullptr);

	if (_tmpStr.empty()) {
		_tmpStr = "";
	}

	return _tmpStr;
}

int DataBase::insertNewGame()
{
	const std::string sql = "INSERT INTO t_games (status, start_time) VALUES('0'," +
		quotesql(std::to_string(time(NULL))) + ");";

	if (sqlite3_exec(this->_db, sql.c_str(), nullptr, 0, nullptr) != SQLITE_OK)
		return -1;

	return sqlite3_last_insert_rowid(this->_db);
}

bool DataBase::updateGameStatus(int gameId)
{
	const std::string sTime = std::to_string(time(NULL));
	const std::string sql = "UPDATE t_games SET status=1, end_time=" + quotesql(sTime);

	return this->executeSQL(sql);
}

bool DataBase::addAnswerToPlayer(int gameId, std::string username, int questionId, std::string answer, bool isCorrect, int answerTime)
{
	const std::string sql = "INSERT INTO t_players_asnwers (game_id, username, question_id, player_answer, is_correct, answer_time) VALUES(" +
		quotesql(std::to_string(gameId)) + "," +
		quotesql(username) + "," +
		quotesql(std::to_string(questionId)) + "," +
		quotesql(answer) + "," +
		quotesql(std::to_string(isCorrect)) + "," +
		quotesql(std::to_string(answerTime)) + ");";

	return this->executeSQL(sql);
}

int DataBase::callbackQuestions(void *, int argc, char ** argv, char ** azCol)
{
	const auto q = new Question(atoi(argv[0]), std::string(argv[1]), std::string(argv[2]),
		std::string(argv[3]), std::string(argv[4]), std::string(argv[5]));
	DataBase::_questions.push_back(q);

	return 0;
}

int DataBase::callbackBestScores(void *, int argc, char ** argv, char ** azCol)
{
	const int count = std::stoi(argv[0]);
	const std::string username(argv[1]);

	DataBase::_tmpStr += (Helper::getPaddedNumber(username.size(), 2) +
		username +
		Helper::getPaddedNumber(count, 6)
		);

	return 0;
}

#include <iostream>

int DataBase::callbackPersonalStatus(void *, int argc, char ** argv, char ** azCol)
{
	const float avg = std::stof(argv[0]);
	std::cout << avg;
	const int corr = std::stoi(argv[1]), wrong = std::stoi(argv[2]),
		wole = static_cast<int>(avg), dec = static_cast<int>(avg * 10) % 10,
		gamesNo = std::stoi(argv[3]);

	std::cout << "NODER";

	_tmpStr = Helper::getPaddedNumber(gamesNo, 4) +
		Helper::getPaddedNumber(corr, 6) +
		Helper::getPaddedNumber(wrong, 6) +
		Helper::getPaddedNumber(wole, 2) +
		Helper::getPaddedNumber(dec, 2);

	return 0;
}

std::string DataBase::quotesql(const std::string& s) {
	return std::string("'") + s + std::string("'");
}

int DataBase::countRows(std::string sql) {
	sqlite3_stmt * stmt;
	if (sqlite3_prepare_v2(this->_db, sql.c_str(), sql.length(), &stmt, NULL) == SQLITE_OK)
	{
		sqlite3_step(stmt);
		return sqlite3_data_count(stmt);
	}

	return 0;
}

bool DataBase::executeSQL(std::string sql) {
	sqlite3_stmt * stmt;
	if (sqlite3_prepare_v2(this->_db, sql.c_str(), sql.length(), &stmt, NULL) == SQLITE_OK)
		return sqlite3_step(stmt) == SQLITE_DONE;

	return false;
}